# -*- coding: utf-8 -*-
"""
LS model in 

"""

from util import *













Question 1 a

#generating 100000 market paths
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_d = 0.025
K_mat = 90.0
K_D = 100.0

npaths = 100000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)

#generate times of 1st arrival of exponential distribution with parameter lambda_d
arriv = exponential_samples(npaths, lambda_d)

#alter arrival times so that for arrival time, t, t <= T, t is rounded to the value in the ts array that is greater
#than it. This matches the pricing in the LS-like algorithm as it assumes that default does not occur at the current time
#For value t >= T, the value is set to be around 1e20, indicating maturity occurs before death
a = np.subtract.outer(arriv, np.append(ts,  1e20 + np.max(arriv)))
idx = np.ma.argmin(abs(np.ma.MaskedArray(a, a >= 0)) , axis = 1)

#calculate payoffs for death occurring at each of the times in the paths matrix, then calculate payoffs for maturity
#for the final time of the paths matrix, and then stack them creating a 122 rows by npaths columns matrix
payoffs = np.vstack([u_D(paths) + ((-1 * alpha) * ts[:,np.newaxis]) , u_mat(paths[-1]) + ((-1 * alpha) * ts[-1])])

#average payoffs over all npaths 
np.mean(payoffs[idx,np.arange(0,len(paths[1]))])




#generating 100000 market paths
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_d = 0.025
K_mat = 90.0
K_D = 100.0

npaths = 100000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)

#create recursive function to calculate C recursively in terms of future values of C
def recursive_C(t):
    if (t == (len(ts) - 1)):
        C = u_mat(paths[t])
    else:
        C = (recursive_C(t+1) * (1-p)) - (dt*alpha) + (u_D(paths[t+1]) * p)
    return C

#calculate increment length, probability of death p, probability of no death (1-p)
dt = np.diff(ts)[0]
p = 1 - np.exp(-(lambda_d * dt))
np.mean(recursive_C(0))










# Question 2 a

#generating 100000 market paths
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_min = 0.005
lambda_max = 0.04
K_mat = 90.0
K_D = 100.0

npaths = 50000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)

#terminal value vector
Y = u_mat(paths[-1])

#matrix to store pw linear regression values
vals = []

for i in range(len(ts)-2, -1, -1):
    dt = ts[i+1] - ts[i]
    #equally spaced knots based on the support points (current stock prices)
    xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
    #calculate the weights for each basis function, vector B, using least squares minimization
    #also returns the piecewise functions 
    ps, fs = pwlinear_fit(paths[i], Y, xknots)
    #calculate the estimated conditional expected value of Y_{t+1} using time t Measurable values using
    #pw linear regression
    est = sum([f(paths[i])*p for (f, p) in zip(fs, ps)])
    Y = np.where((u_D(paths[i]) >= (est - (alpha*dt))), ((1 / ((lambda_max * dt) + 1)) * (est -(alpha * dt) + (u_D(paths[i]) * lambda_max * dt))), ((1 / ((lambda_min * dt) + 1)) * (est -(alpha * dt) + (u_D(paths[i]) * lambda_min * dt))))
    
    vals.append((fs,ps))
np.mean(Y)    








Question 2 b

#generating 100000 market paths
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_min = 0.005
lambda_max = 0.04
K_mat = 90.0
K_D = 100.0

npaths = 100000

#generate paths for lower bound estimate
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)

#flip the stored estimates so it matches order of for loop
vals = np.flip(vals, axis = 0)
#terminal value vector
Y = u_mat(paths[-1])

#using old conditional expectation estimates to decide which lambda value to use, but using the new estimates
#from this run to calculate the conditional expectation estimate in the actual equation
for i in range(len(ts)-2, -1, -1):
    dt = ts[i+1] - ts[i]
    #equally spaced knots based on the support points (current stock prices)
    xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
    #calculate the weights for each basis function, vector B, using least squares minimization
    #also returns the piecewise functions 
    ps, fs = pwlinear_fit(paths[i], Y, xknots)
    #calculate the estimated conditional expected value of Y_{t+1} using time t Measurable values using
    #pw linear regression
    est = sum([f(paths[i])*p for (f, p) in zip(fs, ps)])
    oldest = sum([f(paths[i])*p for (f, p) in zip(vals[i][0], vals[i][1])])
    Y = np.where((u_D(paths[i]) >= (oldest - (alpha*dt))), ((1 / ((lambda_max * dt) + 1)) * (Y -(alpha * dt) + (u_D(paths[i]) * lambda_max * dt))), ((1 / ((lambda_min * dt) + 1)) * (Y -(alpha * dt) + (u_D(paths[i]) * lambda_min * dt))))


np.mean(Y)  







Question 2 c

#generating 100000 market paths
npaths = 100000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)

#calculating price using deterministic max
lambda_d = 0.04
p = 1 - np.exp(-(lambda_d * dt))
max_val = np.mean(recursive_C(0))

#calculating price using deterministic min
lambda_d = 0.005
p = 1 - np.exp(-(lambda_d * dt))
min_val = np.mean(recursive_C(0))

print("Using Constant Deterministic Lambda of 0.04 gives a price of {}".format(max_val))
print("Using Constant Deterministic Lambda of 0.005 gives a price of {}".format(min_val))





Question 3 (Uncertain Mortality Rate Model - Longstaff and Schwartz) In addition to the BSDE simulation, the reinsurance deals can be priced by adapting the Longstaff-Schwartz algorithm.
An implementation of the Longstaff-Schwartz algorithm is provided below for your convenience.
The same lower and upper bounds of λDt
are used as Question 2.

(a). In the example code, we use European put price (the strike is taken to be the average of Kmat
and KD

) and constant as basis functions to estimate the sum of future payoffs. Modify the code by using the piecewise-linear fit to estimate the sum of future payoffs.

(b). Compare the BSDE scheme with the Longstaff-Schwartz-like scheme in terms of variance. For each method run the two-step procedure (regression and indedenpent pricing) 20 times to estimate variance. Use 5000 paths for regression and 50000 for indepedent pricing.

(c). What is approximately the value of the fee α
that makes the deal costless at inception?


# Longstaff-Schwartz algorithm for Uncertain Mortality Rate Model
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_min = 0.005
lambda_max = 0.04

K_mat = 90.0
K_D = 100.0

#I put my changes in the example code. Basically I just replace the sections relating to regression
# first Monte Carlo run to estimate the value function by regressions
npaths = 5000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
vals = []
V = u_mat(paths[-1])
lambd = np.where(u_D(paths[-1]) >= V, lambda_max, lambda_min)
K = 0.5*(K_mat+K_D)
for i in range(len(ts)-2, -1, -1):
    dt = ts[i+1]-ts[i]
    p = 1-np.exp(-lambd*dt)
    V = p*u_D(paths[i+1]) + (1-p)*V - alpha*dt
    #equally spaced knots based on the support points (current stock prices)
    xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
    #calculate the weights for each basis function, vector B, using least squares minimization
    #also returns the piecewise functions 
    ps, fs = pwlinear_fit(paths[i], V, xknots)
    #calculate the estimated conditional expected value of Y_{t+1} using time t Measurable values using
    #pw linear regression
    est = sum([f(paths[i])*p for (f, p) in zip(fs, ps)])
    vals.append((fs,ps))
    lambd = np.where(u_D(paths[i]) >= est, lambda_max, lambda_min)
   
vals = np.flip(vals, axis = 0)

# independent simulation to obtain a lower bound
npaths = 100000
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
V = u_mat(paths[-1])
lambd = np.where(u_D(paths[-1]) >= V, lambda_max, lambda_min)
for i in range(len(ts)-2, -1, -1):
    dt = ts[i+1]-ts[i]
    p = 1-np.exp(-lambd*dt)
    V = p*u_D(paths[i+1]) + (1-p)*V - alpha*dt
    est = sum([f(paths[i])*p for (f, p) in zip(vals[i][0], vals[i][1])])
    lambd = np.where(u_D(paths[i]) >= est, lambda_max, lambda_min)
np.mean(V)





def LSLfcn(alpha):
    npaths = 5000
    paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
    vals = []
    V = u_mat(paths[-1])
    lambd = np.where(u_D(paths[-1]) >= V, lambda_max, lambda_min)
    K = 0.5*(K_mat+K_D)
    for i in range(len(ts)-2, -1, -1):
        dt = ts[i+1]-ts[i]
        p = 1-np.exp(-lambd*dt)
        V = p*u_D(paths[i+1]) + (1-p)*V - alpha*dt
        xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
        ps, fs = pwlinear_fit(paths[i], V, xknots)
        est = sum([f(paths[i])*p for (f, p) in zip(fs, ps)])
        vals.append((fs,ps))
        lambd = np.where(u_D(paths[i]) >= est, lambda_max, lambda_min)
        
    vals = np.flip(vals, axis = 0)
    npaths = 50000
    paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
    V = u_mat(paths[-1])
    lambd = np.where(u_D(paths[-1]) >= V, lambda_max, lambda_min)
    for i in range(len(ts)-2, -1, -1):
        dt = ts[i+1]-ts[i]
        p = 1-np.exp(-lambd*dt)
        V = p*u_D(paths[i+1]) + (1-p)*V - alpha*dt
        est = sum([f(paths[i])*p for (f, p) in zip(vals[i][0], vals[i][1])])
        lambd = np.where(u_D(paths[i]) >= est, lambda_max, lambda_min)
    return np.mean(V)





S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_min = 0.005
lambda_max = 0.04

K_mat = 90.0
K_D = 100.0

#calculating the results of 20 runs
BSDEresults = [BSDEfcn(3.0) for i in range(0,20,1)]
LSLresults = [LSLfcn(3.0) for i in range(0,20,1)]

#outputting variance and mean
print("Unbiased Sample Variance Estimate for BSDEs = {}".format(np.var(BSDEresults, ddof=1)))
print("Unbiased Sample Variance Estimate for LS-L algorithm = {}".format(np.var(LSLresults, ddof=1)))
print("Average of BSDEs = {}".format(np.mean(BSDEresults)))
print("Average of LS-L algorithm = {}".format(np.mean(LSLresults)))




The BSDE prices and LS-L prices had similar variance,
usually with the BSDE pricing having slightly higher
(but still not very big in the context of the generally low numbers here).
This is likely due to the fact that the BSDE pricing is similar to TVR pricing of American options,
 where the estimate of the future value is calculated as each time and used
 for the next step's calculation and so on, thus carrying over any errors
 through the time steps. In contrast, the LS-L algorithm only selects a lambda value at a time tk and uses that lambda value in order to calculate the likelihood of death when estimating the future cashflows (so averaging over default events) at time tk−1. This is like in the LS algorithm for American options where the regression estimates of the future value of the option isn't used, rather just the optimal time of future cash flows is estimated and the cash flows left the same.





Question 3 c

def LSLfcnx20(alpha):
    return np.mean([LSLfcn(alpha) for i in range(0,20,1)])
def BSDEfcnx20(alpha):
    return np.mean([BSDEfcn(alpha) for i in range(0,20,1)])

fair_fee_LSL = scipy.optimize.brentq(LSLfcnx20, 3.379,3.389)
#scipy.optimize.brentq(BSDEfcnx20, 3.35,3.4)

print("The approximate fair fee per year, alpha, using the LSL algorithm is {} USD".format(fair_fee_LSL))
print("The price using {} as the fee is approximately (using 20 runs) {} USD".format(fair_fee_LSL, LSLfcnx20(fair_fee_LSL)))







"""
Question 4. Set KD=0, and all the other parameters remain the smae, in particualr α=3. Then the default event is equivalent to lapse (with no mortality payoff). It has been observed that insurance subscribers do not lapse optimally, which explains the use of the uncertain lapse model with minimum and maximum lapse rates λ−−, λ¯¯¯

(a). If insurance subscribers were to lapse optimally, i.e., exercise optimally their option to lapse, how would you price the contract?

(b). Explain how you can get this price (from optimal exercise) in the uncertain lapse model. Reuse the code provided in Question 3 to show your result (you may have to change some parameters in the code).

(c). Implement Longstaff-Schwartz algorithm to price the American option with payoff uD=0
(Always perform a second indepedent run to get a clean lower bound price). Compare the price from uncertain lapse model in part (b) with the American price.
"""





#######################################################################################
"""
Question 4 b

Since the optimal lapse value of the option is such that subscribers only lapse on the condition that uD>u
, then we can price the option using the Longstaff Schwartz like algorithm, by making it such that whenever uD>u, that lapse occurs. In the context of the Poisson process, making the arrival occur with probability 1 is equivalent to having an infinite parameter, and so we can set λ¯D=∞, to 'force' lapsing. Equivalently whenever the condition uD>u is not fulfilled, we want to ensure no lapse occurs, and this is equivalent to setting λ−−D=0. (Note that the code in Question 3, uses the condition uD≥u
, but this does not affect the pricing)
"""

S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
alpha = 3
lambda_min = 0
lambda_max = math.inf

K_mat = 90.0
K_D = 0

LSLfcn(3)





#######################################################################################
# Question 4 c

#longstaff schwartz algorithm for American pricing
npaths = 50
S = 100
vol = 0.3
T = 10
ts = np.linspace(0, T, int(np.round(T*12))+1)
dt = np.diff(ts)[0]
K_mat = 90.0

alpha = 3



paths = blackscholes_mc(S=100.0, vol=0.3, r=0, q=0, ts=ts, npaths=npaths)

def cf_future_approx_poly(paths, ts,)
    #create array that holds Polynomial coefficients from LS, put into vals
    vals = []
    T = len(ts)-2
    payoff = u_mat(paths[-1], K_mat)
    for i in range(T, -1, -1):
        payoff = payoff - (alpha*dt)
        xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
        ps, fs = pwlinear_fit(paths[i], payoff, xknots)
        if i == (T):
            contval = payoff
        else:
            contval = sum([f(paths[i])*p for (f, p) in zip(fs, ps)])
    
        exerval = (np.ones_like(paths[1,:], dtype=np.float) * 0.0) - (alpha * dt)
        is_lapse = exerval >= contval
        payoff[is_lapse] = exerval[is_lapse]
        vals.append((fs,ps))
    
    vals = np.flip(vals, axis = 0)
    return vals





# independent simulation to obtain a lower bound
T = len(ts)-2
npaths = 100
paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)


def option_price(paths, ts,) :
    payoff = u_mat(paths[-1], K_mat)  # Final Value
    for i in range(T, -1, -1):        # Backward
        payoff = payoff - (alpha*dt)
        if i == (T):
            contval = payoff
        else:
            ## Polynomial value
            contval = sum([f(paths[i])*p for (f, p) in zip(vals[i][0], vals[i][1])])
    
    
        exerval = (np.ones_like(paths[1,:], dtype=np.float) * 0.0) - (alpha * dt)

        ## Lapse condition
        is_lapse = exerval >= contval
        payoff[is_lapse] = exerval[is_lapse]
    
    mc_mean = np.mean(payoff)
    return mc_mean







When performing the pricing of the deal as if it were an American option, when using 5000 initial and 100000 subsequent runs for the low biased pricing, the price was around 3.6
USD, as compared to the Longstaff-Schwartz like algorithm giving a price of around 3.65

USD when using a comparable number of runs in the simulation.

A few adjustments had to be made to the Longstaff Schwartz algorithm for American options (LSA) to price the reinsurance deal properly. For example the payoff of exercising at all times had to become 0
, indicative of the lapse value, and also since the reinsurance deal Longstaff Schwartz-like algorithm made the assumption of no exercise/lapse occurring on the current date, the exercise value in the LSA algorithm had to be 0

less the monthly fee. Since there was now a fee involved, the LSA algorithm also had to be modified so that the payoff was decreased by the monthly fee each time step.

The main difference between the two algorithms, was that the LS-like algorithm for reinsurance deals calculates the value on the date tn−1
using the information directly from the date tn information, and in general, estimating lambda for the previous time using regression on the value calculated at the present time. On the other hand, the LSA algorithm doesn't have the 'looking into the future' issue and always estimates future payoffs using the regression, and performs the lapse/exercise decision using regression. This results in a slightly higher price using the LS-like algorithm. I tried letting the LSA algorithm look into the future at time tn−1 as well, by letting the continuation value equal the actual future payoff, and this resulted in even more similar prices between the two algorithms.



















