# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
import math
import scipy


####################################################################################################
VERBOSE = 1

def log(*arg) :
  if VERBOSE :
     for x in arg :
       print( x, globals().get(x), flush=True)


from util import *



####################################################################################################
####################################################################################################
def state_calc(t, paths_t, die_path, params):
    # calcualte system tate as dict
    state = {
        "discount": paths_t[:, 0],
        "st": paths_t[:, 2],
        "bt": paths_t[:, 1],
        "is_die": die_path,

        "age"     : params["age"] + t,
        "premium" : params['premium'],
        "msample" : params["msample"],

        "t"  :  t,  # current time
        "T"  :  params['T'],  # maturity
        "dT" :  params["T"] - t,
    }
    if VERBOSE :
      print(state)
    return state





def cf_calc(t, T, st, bt, dd=[],is_die=[], is_lapse=[]):
    """
     Discount factor dd
      ## Provide Raw Cash float at ti with st price
    """
    if t == T:   return cf_atmaturity(st, params)
    elif t > 0:  ### All samples
        cf = np.zeros_like(st)
        for k in range(len(st)) :
            if is_die[k]  :   cf[k] = dd[k] * (150.0 + np.maximum(0, st[k]  - 100.0) )
            if is_lapse[k]  : cf[k] = dd[k] * cf_atlapse(st, params)
            else  :           cf[k] = dd[k] * 1.0
    return cf



def cf_calc_atlapse( t, T, state=None, params=None ):
    """
       ## Provide Raw Cash float at ti with st price 
       dT : Time to maturity
       Discount factor dd    
    """
    st, bt, dd, is_die = state['st'], state['bt'], state['discount'], state['is_die']

    #is_die = is_die if is_die is not None else np.zeros_like( st)
    
    if   t >= T  :  cf = 100.0 + np.maximum(0, st  - 100.0 )   ### Full Cash Flow

    elif t == 0  :  cf = np.zeros_like(st, dtype="float32")  ### Initial : No Lapse, nothinh

    else :  ### All samples
        cf = np.zeros(len(st), dtype= "float32" )
        # print( st )
        for k in range( len(cf) ) :
            if is_die[k] == 1 :  cf[k] =  (150.0 + np.maximum(0.0 , st[k] - 100.0) )
            else              :  cf[k] =  np.maximum(0.0 , st[k] - params["strike"]  )  ##Lapse
    return cf



def cf_atlapse(st, params):
    v = np.maximum(st - params["strike"] , 0.0 )
    return v
    
    
def cf_atmaturity(st, params):
    v = 100.0 + np.maximum(st - 100.0, 0.0 )
    return v
   


def lapse_cond(t, T, cf0=0.0, cf_future=0.0, state={}):
    """
      #### return 1 or 0 based on the  Lapse criteria
      ( cf0 > cf_future ) * age_bucket

    """
    age        = state.get("age")
    age_adjust = {0: 1.0, 50: 0.5}.get(age)
    is_die     = state["is_die"]

    if t >= T :
        is_lapse_list = np.zeros_like( cf0)  + 2   
        return is_lapse_list
    
    is_lapse_tmp =  ( cf0 > cf_future )
    ### Remap with only one zero per line
    is_lapse_list = np.zeros_like( is_lapse_tmp, dtype="int16")
    for j in range(  is_lapse_list.shape[0] ) :  ### All samples
            if is_die[j] == 1   :  is_lapse_list[  j ] = 3  ### Die
                  
            elif is_lapse_tmp[j] : is_lapse_list[  j ] = 1 

    return is_lapse_list


#### Option Price
def option_calc(cf_list,paths, params, strike=100.0) :
    dd = paths[:,0,:][0] 
    cfdis = np.zeros_like( cf_list, dtype=float)
    for t in range(0, params["T"]+1) :  
      cfdis[:, t] = 0.01*dd[t]*np.maximum(cf_list[:, t] - strike, 0.0)
    
    price = np.sum(cfdis) / len(cfdis) 
    return price


def duration_calc( cf_islpase_list ) :
  time_lapse = np.where( cf_islapse_list  > 0.0 )[1] 
  return pd.DataFrame({ "time" : time_lapse  })




#################################################################################################
def generate_cf_calc( npath, lapse_cond, cf, proxy_choice="table",
                      params={"age": 40.0, "premium": 100.0}, proxy=None, proxy_pars={}):
    """
       Generate cash Flows, Lapse exercice from the rules and aprroximation choice
          t=0 (initil), t=1,, t=2    n=3  2 years maturity
          t=0, t=1, t=2, ...t=5      n=6  5 years maturity
              
       npath        = 5
       proxy_choice = "const"
       kwargs       = proxy_pars
       t = T-1      : Indice of array  AND indice of time
    """
    params["msample"] = npath
    T                 = int( params['T'] )
        
    ## Choice of approximation for E[..]  ######################################################
    if proxy is None:
        proxy = proxy_function(proxy_choice, **proxy_pars, **params)
        paths = generate_path(proxy_pars["npath"], params)  #### Fits the approximator #########
        proxy.fit(paths, params)


    #### Simulate Cash Flow in Vector forms : all samples at same time, Vector form  ##########
    paths  = generate_path_all(npath, params)   # Discount, Bond, Stock Prices
    #paths        = np.zeros((npath, 4, T+1 ), dtype="float32")
    #paths[:,0,:] = generate_path_discount(npath, params)     
    #paths[:,1,:] = generate_path_bond(npath, params)  
    #paths[:,2,:] = generate_path(npath, params)[:,0,:]   ## Stock Prices
    die_path     = generate_path_die(npath, params)  # mortality
    # print( paths.shape, die_path.shape )

    
    def toz(): return [None]*(T+1)    
    cf_list, cf_future, cf_islapse, state_list, cf_today = toz(), toz(), toz(), toz(), toz()


    #####    [T, 1]  --> [T-1, 1] framework
    ### t=0 No stochastic price, t=1 : stochastic pric
    for t in range(0, T+1, 1):  # index in [0, T-1]
        print("\n", t, "################ State of the system. ###############################")
        state              = state_calc(t, paths[:, :, t], die_path[:,t], params)
        st, bt, is_die, dd = state["st"] , state["bt"], state["is_die"], state["discount"]   ## Stock price vector, Bond price vector from brownian, ## Vector : 1 or 0 from Binomial
        zeros              = np.zeros_like(st)
        state_list[t]      = state
        print( "bt", bt )

        if t == 0 :
          cf_today[t]    =  zeros
          cf_future[t]   =  zeros # proxy.predict_cf(state, params)
          cf_islapse[t]  =  zeros              
          cf_list[t]     =  zeros
          
        elif t >= T :  ## T as maturity
          cf_today[t]    =  cf_atmaturity(st, params)
          cf_future[t]   =  zeros
          cf_islapse[t]  =  zeros + 2  ## T as maturity
          cf_list[t]     =  cf_atmaturity(st, params)
          
        else :   #### Cash Flow  t>0
          cf_today[t]   = cf_calc_atlapse( t, T, state, params) # Vector (m samples)
          cf_future[t]  = proxy.predict_cf(state, params)                              # Vector, Discount Cash Flow Approximation  (m samples  )
          cf_islapse[t] = lapse_cond(t, T, cf_today[t], cf_future[t], state)           # Vector (m samples)
          cf_list[t]    = np.where(cf_islapse[t], cf_today[t] , zeros)
          print("cft",       cf_today[t] )
          print("cf_future", cf_future[t] )

         
    cf_islapse = normalize_event_flag(cf_islapse)    
    cf_list    = normalize_event_flag(cf_list)   
    return ( cf_list , cf_islapse, to_format( cf_future ), 
             to_format( cf_today), paths, die_path)



####################################################################################################
# cf_future_list    [ ntime, msample]
# cf_islapse_list   [ ntime, msample]




###################################################################################################
params = { "age": 40.0, "premium": 100.0,
           "proba_death" : 0.015,
                        # Discount,  Stock Price,  
           "s0"     :   np.ones((3)) * 100.0,
           "vol0"   :   np.array([[ 0.20, 0.1,  0.01 ]]).T ,
           "drift"  :   np.array([[ 0.01, 0.01,  0.01 ]]).T ,
           "corr"   :   np.array( [[ 1.0, 0.0, 0.0], [ 0.0, 1.0, 0.0] ,[ 0.0, 0.0, 1.0]   ] )  ,
           "T"      :   15,
           "msample" :  13,
           "rate" : 0.02,
           
           "strike" : 100.0
         }

proxy_pars = {"npath": 4}


VERBOSE = 1
cf_list, cf_islapse_list, cf_future_list, cf_today_list, paths, die_paths = generate_cf_calc(3, lapse_cond, cf_calc,
                             proxy_choice="const",  params=params, proxy=None, proxy_pars=proxy_pars)


  
####################################################################################################
######  Polynome Regression ########################################################################
params = { "age": 40.0, "premium": 100.0,
           "proba_death" : 0.05,
                        # Discount,  Stock Price,  
           "s0"     :   np.ones((3)) * 100.0,
           "vol0"   :   np.array([[ 0.20, 0.1,  0.01 ]]).T ,
           "drift"  :   np.array([[ 0.01, 0.01,  0.01 ]]).T ,
           "corr"   :   np.array( [[ 1.0, 0.0, 0.0], [ 0.0, 1.0, 0.0] ,[ 0.0, 0.0, 1.0]   ] )  ,
           "T"      :   5,
           "msample" :  5,
           "rate" : 0.02,
           
           "strike" : 100.0
         }


proxy_pars = {"npath": 4}  ### Low number of paths is better 


VERBOSE = 1
cf_list, cf_islapse_list, cf_future_list, cf_today_list, paths, die_paths = generate_cf_calc(700, lapse_cond, cf_calc,
                                           proxy_choice="polynom",  params=params, proxy=None, proxy_pars=proxy_pars)




####################################################################################################
######  Polynome Regression ########################################################################
params = { "age": 40.0, "premium": 100.0,
           "proba_death" : 0.001,
                        # Discount,  Stock Price,  
           "s0"     :   np.ones((3)) * 100.0,
           "vol0"   :   np.array([[ 0.20, 0.20,  0.20 ]]).T ,
           "drift"  :   np.array([[ -0.10, -0.05,  -0.05 ]]).T ,
           "corr"   :   np.array( [[ 1.0, 0.0, 0.0], [ 0.0, 1.0, 0.0] ,[ 0.0, 0.0, 1.0]   ] )  ,
           "T"      :   3,
           "msample" :  1000,
           "rate" :   0.05,
           
           "strike" : 100.0
         }


proxy_pars = {"npath": 100}  ### Low number of paths is better 


VERBOSE = 0
cf_list, cf_islapse_list, cf_future_list, cf_today_list, paths, die_paths = generate_cf_calc(1350, lapse_cond, cf_calc,
                                           proxy_choice="polynom",  params=params, proxy=None, proxy_pars=proxy_pars)


option_calc(cf_list,paths, params, strike=100.0) 




duration_calc( cf_islapse_list ).hist() 


timeMaturity / 



"""
   8.94   20k paths  strike 100
   5.97   20k paths


5.76 : option price
10.62 : striek 90
2.10 : strik 110




"""





####################################################################################################
###### Option Price for check

2 90 8.072
2 100 13.895
2 110 21.353


Table 1: Summary results for max-call options on d symmetric assets for parameter values
of r = 5%, δ = 10%, σ = 20%, ρ = 0, K = 100, T = 3, N = 9. tL is the number
of seconds it took to train τ
Θ and compute Lˆ. tU is the computation time for
Uˆ in seconds. 95% CI is the 95% confidence interval (19). The last column lists
values calculated with a binomial lattice method by Andersen and Broadie (2004)
for d = 2–3 and the 95% confidence intervals of Broadie and Cao (2008) for d = 5.


Sit = si0.exp[r − δi − σi2 /2.t + σiWit, i = 1, 2... ]

Max( exp(-rt) max(st-k)









####################################################################################################
######  Polynome Regression ########################################################################
"""
2 90 8.072
2 100 13.895
2 110 21.353


Table 1: Summary results for max-call options on d symmetric assets for parameter values
of r = 5%, δ = 10%, σ = 20%, ρ = 0, K = 100, T = 3, N = 9. tL is the number

Andersen and Broadie (2004)  , binomial

"""
params = { "age": 40.0, "premium": 100.0,
           "proba_death" : 0.0001,
          
           # Discount,  Stock Price,  
           "s0"     :   np.ones((3)) * 100.0,
           "vol0"   :   np.array([[ 0.20, 0.20,  0.20 ]]).T ,
           "drift"  :   np.array([[ -0.05,  -0.05,   -0.05 ]]).T ,
           "corr"   :   np.array( [[ 1.0, 0.0, 0.0], [ 0.0, 1.0, 0.0] ,[ 0.0, 0.0, 1.0]   ] )  ,
           "T"      :   3,
           "msample" :  5,
           "rate"   :   0.05,
           "strike" :   100,
         }

proxy_pars = {"npath": 10000}  ### Low number of paths is better 


VERBOSE = 1
cf_list, cf_islapse_list, cf_future_list, cf_today_list, paths, die_paths = generate_cf_calc(700, lapse_cond, cf_calc,
                                           proxy_choice="poly",  params=params, proxy=None, proxy_pars=proxy_pars)











####################################################################################################
########  Neural Network ########################################################################### 
params = { "age": 40.0, "premium": 100.0,
           "proba_death" : 0.015,
                        # Discount,  Stock Price,  
           "s0"     :   np.ones((3)) * 100.0,
           "vol0"   :   np.array([[ 0.20, 0.1,  0.01 ]]).T ,
           "drift"  :   np.array([[ 0.01, 0.01,  0.01 ]]).T ,
           "corr"   :   np.array( [[ 1.0, 0.0, 0.0], [ 0.0, 1.0, 0.0] ,[ 0.0, 0.0, 1.0]   ] )  ,
           "T"      :   5,
           "msample" :  5,
           "rate" : 0.02,
           
           "strike" : 100.0,
           "prefix" : "test"
         }


def cf_atlapse(st, params):
    v = np.maximum(st , 0.0 )
    return v
    
    
def cf_atmaturity(st, params):
    v = 100.0 + np.maximum(st-100.0, 0.0 )
    return v
    
proxy_pars = {"npath": 11}  ### Low number of paths is better 


VERBOSE = 1
cf_list, cf_islapse_list, cf_future_list, cf_today_list, paths, die_paths = generate_cf_calc(9, lapse_cond, cf_calc,
                          proxy_choice="nn",  params=params, proxy=None, proxy_pars=proxy_pars)







 
np.exp(0.22)
 
 

####################################################################################################
####################################################################################################
### NN modeling

from submodels import *


dd = {}
for t in range(T-1, -1, -1) : 
  y = 1
  X = 1
  filename = "models/{t}_nn/nn_model.pkl"
  os.makedirs( filename ) 
  model, loss_histo = fit(data=None, model=None, epochs=1, 
                          batch=1,   filename= filename)
  dd[t] = model 







%load_ext autoreload
%autoreload 2
%reload_ext autoreload


reload(nn)







class LossHistory(cb.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        batch_loss = logs.get('loss')
        self.losses.append(batch_loss)
        
        
        
        

def load_data():
    print 'Loading data...'
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    X_train /= 255
    X_test /= 255

    y_train = np_utils.to_categorical(y_train, 10)
    y_test = np_utils.to_categorical(y_test, 10)

    X_train = np.reshape(X_train, (60000, 784))
    X_test = np.reshape(X_test, (10000, 784))

    print 'Data loaded.'
    return [X_train, X_test, y_train, y_test]
  
  
  
  




"""
TODO  :
      Price some american Option Bermudan as benchmark.
      Polynomial exercie



def cf_calc(t, T, st, bt, dd=[],is_die=[], is_lapse=[]):
    """
     Discount factor dd
      ## Provide Raw Cash float at ti with st price
    """
    if t == T:  cf = 100.0 + np.maximum(0, st / 100.0 - 1)

    elif t > 0:  ### All samples
        cf = np.zeros_like(st)
        for k in range(len(st)) :
            if is_die[k]  :   cf[k] = dd[k] * (150.0 + np.maximum(0, st[k] / 100.0 - 1) )
            if is_lapse[k]  : cf[k] = dd[k] * np.maximum(0, st[k] / 100.0 )
            else  :           cf[k] = dd[k] * 1.0
    return cf



"""




























####################################################################################################
####################################################################################################
def option_price_02(npath, lapse_cond, cf, proxy_choice="", params={}, proxy=None, proxy_pars={}):
    """
      Option pricing from cash flow
    """
    state, cf_list, _, _ = generate_cf_calc(npath, lapse_cond, cf, proxy_choice=proxy_choice,
                                            params={"age": 40.0, "premium": 100.0},
                                            proxy=None, proxy_pars={})

    discount = state['discount']
    cf_total = np.zeros(params['npath'])
    for k in range(params['npath']):
        cft = 0
        for t in range(params['T']):
            cft = cft + cf[k, t] * discount(k, t)
        cf_total[k] = cft

    m = np.mean(cf_total)
    ms = np.stddev(cf_total)
    return m, ms





####################################################################################################
####################################################################################################


""""

   if name== "zero" :   # Zero correl
        s0    = np.ones((nasset)) * 100.0
        drift = np.ones((nasset,1)) * 0.0
        vol0  = np.ones((nasset,1)) * 0.2      
        vol0 =  np.array([[ 0.20, 0.15, 0.08 ]]).T 
        correl =  np.array([[100, 0 , 0 ],
                       [0,  100, -0 ],
                       [-0,  -0, 100 ],                ])*0.01




Self Supervised learning
Reinforcement Learning


Optimal Policy estimation using Neural Network and self-supervised apporach, application in lapse evaluation.







Estimation of Lapse : 
    relies 
   Actual financial impact problem,

   We decide to review a novel method to estimate and simulate Lapse for insurance related product.
   and compare wtih existing approach.


   Expensive

  User have sub-optimla (reason being retail vs instituatil), long term maturity, the impact can be signiticstive
  in the balance sheet, In addition, regulatory and ALM regulation pushes to provide better estimation of Lapses.


  LS : cannot handle a large number of factors (Exponential),
       Compute burden happens.
        Linearity of the basis function.


  LSTM 

A large number of users
Combine categorical and numerical data (conditional )




Backward induction




"""




