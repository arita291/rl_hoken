# -*- coding: utf-8 -*-
# %matplotlib inline



import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
import math
import scipy




VERBOSE = 1

def log(*arg) :
  if VERBOSE :
     for x in arg :
       print( x,  flush=True)


"""
def to_format(a) :
    return np.array( list( reversed(a)) ).T
"""


def to_format(a) :
    return np.array( a ).T


def normalize_event_flag(v, tranpose=0) :
    ## Put only ONE zeros at start of lenght
    v = np.array(v).T
    v2 = np.zeros_like(v, dtype="int16" )
    for k in range(v.shape[0]) :
        for t in range( v.shape[1]) :
           if v[k,t] > 0 :
               v2[k, t] = v[k,t]
               break
    return v2        
            






####################################################################################################
def proxy_function(choice, **kwarg):
    """ Approximator of E[ ..]
       Const, neural network or table or polynomial
       Q Fit wiht Nueral Netowrk  Deep Reinforcement Learning
    """
    if choice == "const":   proxy = proxy_const(kwarg)
    if choice == "table":   proxy = proxy_table(kwarg)
    if choice == "polynom": proxy = proxy_polynomial(kwarg)
    if choice == "nn":      proxy = proxy_nn(kwarg)
    return proxy



class proxy_const(object):
    ### USe a const o calcilate the lapse /expected value m Cash Flow
    def __init__(self, proxy_params=None, params=None):
        self.model = "const"
        self.params = params

    def fit(self, paths, params):
        self.paths = paths

    def predict_cf(self, state, params):
        #  m = 10 ; dT= 7
        m, dT, t  = state['msample'], state['dT'],  state["t"]
        proba_lapse   = 0.6
        st            = state['st'] 
        st            = st *( np.random.random(len(st)) * 7.0 + 0.8 )
        dd =  np.exp(- 0.02 *t)   ### Discount

        cf = proba_lapse * st * dd  + (1.0-proba_lapse) * dd * (100.0 + np.maximum(st -100.0, 0.0) )
        return cf



class proxy_polynomial(object):
    def __init__(self, proxy_params=None, params=None):
        def __init__(self) :
          self.model         = None
          self.params        = params
          #self.cf_atlapse    = params["cf_atlapse"]
          #self.cf_atmaturity = params["cf_atmaturity"]
          """
           ST-1  predict V(T)
           ....
           S1    predict V(2) 
          """

    def fit(self, paths, params):
        """
           for ti [T-1, 1] :
              E[Vi+1 ] = F(Si)  F is polynomial.
        """
        print("start proxy fit")
        from numpy.polynomial import Polynomial as model
        
        T = params["T"]
        self.model_list, self.fit_result = {},  {}

        ###  T-1  models,, 1-->2, ..., T-1-->T 
        cashflow = cf_atmaturity( paths[:, 1, T] , params)  ## At maturity
        for t in range(T-1, 0, -1):
            print(t)
            st = paths[:, 1, t]
                        
            exercise         = cf_atlapse(st, params) # exercise value for time t[i]
            itm              = exercise > 0.0      # boolean index of all in-the-money paths
            
            fitted           = model.fit( st[itm], cashflow[itm], 2) # fit polynomial of degree 2
            continuation     = fitted(st)   # approximate continuation value
            
            ex_idx           = itm & (exercise > continuation)  # boolean index where exercise is beneficial                        
            cashflow[ex_idx] = exercise[ex_idx]   # update cashflows with early exercises
            
            self.model_list[t] = fitted 
            self.fit_result[t] = (cashflow.copy(), st, fitted, continuation, exercise, ex_idx)
        
        if VERBOSE : print(self.model_list)


    def predict_cf(self, state, params):
        ### T-1 models BUT T time steps
        t, st   = state["t"], state["st"]
        contval = self.model_list[ t ](  st )   
        return contval

        
    def ztest(self): 
        """
        paths    = generate_path(npath, params)
        die_path = generate_path_die(npath, params)  # mortality
        T        = int( params['T'] )
        
        params["cf_atlapse"]    = cf_atlapse
        params["cf_atmaturity"] = cf_atmaturity
        
        
        proxy = proxy_polynomial( proxy_pars, params)
        proxy.fit(paths, params)
        print( proxy.model_list )
        print( proxy.fit_result)
        
        proxy.predict_cf( state, params )
        
        
        proxy_polynomial
        state = {"st" : paths[:, 2, 2], "t" : 2 }
        
        """




class FF(object):
    """  ###### One FeedFoward per time step t     
            X = (s1, s1**2, ..s1**3,..., exp(-S1),.., ,Dt, K, )
            y = ExerccieVal ( Max(S1-100)+ )    
           y1 = 1 or 0
            Indept part. 
       
       LSTM at each step :  (next time)
           yt From backward and local estimate
           LSTM at each time step t
           Total Loss           
           Use use  keras MLP or  TF
                     
    """
    def __init__(self, proxy_params=None, params=None):
          self.params        = params
          self.proxy_params  = proxy_params
          self.model_list    = {}
          self.loss_history = {}


    def fit(self, st, itm, t, params):   
       ## One FF per network.
       import submodels as nn
       import os
       
       folder = f"models/{params['prefix']}/{t}_nn/"
       os.makedirs( folder , exist_ok= True) 
       filename = f"{folder}/nn_model.pkl"

       st = np.log( st*0.01 )
       X  = np.vstack((st, st*st, st*st*st)).T
       
       y = np.log(itm*0.01)
       # y  = np.array( [[0,1  ] if x else [1,0] for x in itm  ])       
       # print( st[itm].shape, cashflow[itm].shape )
       print("X, y", X, y )   ### (nsample, 3)
       
       
       model                = self.model_list.get(t)
       model                = nn.init_model2() if model is None else model
       print( X.shape, y.shape )
       
       model, loss_history  = nn.fit( X=X, y=y, model= model, filename= filename )
       self.model_list[t]   = model
       self.loss_history[t] = loss_history       
       
       
    def predict(self, st, t):
      #### Feature engineering
      st = np.log(st*0.01)
      X  = np.vstack((st, st*st, st*st*st)).T
      print(X.shape)
      
      #### Prediction
      X_pred = self.model_list[t].predict(X, batch_size=16)   # 2D output
      X_pred = 100.0 * np.exp( X_pred ).ravel()  ## Flatten
      
      #### For Binary Prediction      
      # X_pred = np.array( [ 1 if x[1] == 1 else 0 for x in X_pred  ] )  ### 1D output  
      return X_pred






class proxy_nn(object):
    """
       Input X :  S**, S**2, S**3, S**4       
       Output y : Continuation values
    
    """
    def __init__(self, proxy_params=None, params=None):
          self.model         = None
          self.params        = params
          """
           ST-1  predict V(T)
           ....
           S1    predict V(2) 
          """

    def fit(self, paths, params):
        """
           for ti [T-1, 1] :
              E[Vi+1 ] = Fi(Si)  Fi is polynomial.
              (F1, F2, ..., Fn) is stacked FFnetwork.                          
        """
        print("start proxy fit")
        
        self.model      = FF(params)
        T               = params["T"]
        self.fit_result = {}

        ###  T-1  models,, 1-->2, ..., T-1-->T 
        cashflow = cf_atmaturity( paths[:, 1, T] , params)  ## At maturity
        for t in range(T-1, 0, -1):
            print(t)
            st = paths[:, 1, t]
            exercise         = cf_atlapse(st, params) # exercise value for time t[i]
            itm              = exercise > 0.0      # boolean index of all in-the-money paths

            """
            ##### Binary Prediction
            st = np.log( st[itm] )
            X  = np.vstack((st, st*st, st*st*st)).T
            y  = np.array( [[0,1  ] if x else [1,0] for x in itm  ])       
            # print( st[itm].shape, cashflow[itm].shape )
            print(X.shape, y.shape )   ###(nsample, 3)
            # self.model.fit( X, y, t, params) # fit polynomial of degree 2
            """            
            self.model.fit( st[itm], cashflow[itm], t, params) # fit polynomial of degree 2
                        
            continuation       = self.model.predict(st, t)   # approximate continuation value            
            ex_idx             = itm & (exercise > continuation)  # boolean index where exercise is beneficial                        
            
            print("Continuation", continuation, cashflow[itm])
            print("ex_idx", ex_idx.shape, continuation.shape, exercise.shape, cashflow.shape)
            cashflow[ex_idx]   = exercise[ex_idx]   # update cashflows with early exercises            
            self.fit_result[t] = (cashflow.copy(), st, continuation, exercise, ex_idx)
        
        if VERBOSE : print(self.model.model_list)


    def predict_cf(self, state, params):
        ### T-1 models BUT T time steps
        t, st   = state["t"], state["st"]
        contval = self.model.predict( st, t )   
        return contval




class proxy_table(object):
    ### USe a table to calcilate the lapse /expected value
    def __init__(self, model_dict):
        self.model = None
        if model_dict is None:
            self.model_dict = {"ix":    list(np.arange(0, 10.0, 1.0)),
                               "iy":    [0.6, 0.8, 0.9, 1.0, 1.1, 1.2, 1.4],
                               "array": np.array([[0.1, 0.1, 0.1, 0.2],
                                                  [0.1, 0.1, 0.1, 0.2],
                                                  [0.1, 0.1, 0.1, 0.2],
                                                  ])
                               }
        else:
            self.model_dict = model_dict

    def fit(self, paths):
        pass

    def predict(self, state):
        m = state['msample']

        proba_lapse = 0.3
        return np.random.Binomial(1, proba_lapse, m)

        is_lapse_list = []
        for k in range(0, m):
            ### Usage
            factor_01_list = [int(state.t)]  # time to maturity
            factor_02_list = np.round(state.S0 / 100.0, 2)  # Vector of moeyness

            M = self.model_dict['array']
            iM = self.model_dict['ix']
            jM = self.model_dict['iy']
            for i in range(M.shape[0]):
                for j in range(M.shape[1]):
                    if jM[i] < ix < jM[j + 1]:  ##3 Condition proba
                        pass

            is_lapse_list.append(np.random.Binomial(1, lapse_rate, 1))

            #### Reverse lapse value --> Decision Value

        """
        is_lapse      =  np_binomial_array(m, dT-1, proba_lapse )
        ### Cash Flow  ################################################

        for k in range(m) :
          for t in range(0, dT-1) :
            st = paths[:,t,0]

            if t == dT-1 :
                cf_sum = 100.0 + max(st/100-1, 0.0); break

            if is_lapse[t] == 1 :
                cf_sum = max(st[k]/100.0,0); break

            if is_die[t] == 1 :
                cf_sum = 150.0 + max(st[k]/100.0-1, 0.0); break


        cf[k] = cf_sum
        """



def cf_atlapse(st, params):
    v = np.maximum(st , 0.0 )
    return v
    
    
def cf_atmaturity(st, params):
    v = 100.0 + np.maximum(st-100.0, 0.0 )
    return v
    






####################################################################################################
####################################################################################################
def option_price_01(paths, ts, ):
    # independent simulation to obtain a lower bound
    payoff = u_mat(paths[-1], K_mat)  # Final Value
    for i in range(T, -1, -1):  # Backward
        payoff = payoff - (alpha * dt)
        if i == (T):
            contval = payoff
        else:
            ## Polynomial value
            contval = sum([f(paths[i]) * p for (f, p) in zip(vals[i][0], vals[i][1])])

        exerval = (np.ones_like(paths[1, :], dtype=np.float) * 0.0) - (alpha * dt)

        ## Lapse condition
        is_lapse = exerval >= contval
        payoff[is_lapse] = exerval[is_lapse]

    mc_mean = np.mean(payoff)
    return mc_mean



def plot_approx_n(n_steps, ax):
    cashflow, x, fitted, continuation, exercise, ex_idx = intermediate_results[n_steps]
    fitted_x, fitted_y = fitted.linspace()
    y_min, y_max = running_min_max(cashflow, exercise, fitted_y)
    offset = 0.1 * (y_max - y_min)
    ax.set_ylim((y_min - offset, y_max + offset))
    ax.plot(x, cashflow, '^', color='green', zorder=3);
    ax.plot(x[ex_idx], exercise[ex_idx], 'x', color='red', zorder=5);
    ax.plot(x[~ex_idx], exercise[~ex_idx], 'x', color=grey, zorder=4);
    ax.plot(fitted_x, fitted_y, zorder=2);
    _x = np.linspace(np.min(x), np.max(x))
    ax.plot(_x, fitted(_x), '--', color=grey, zorder=1);
    ax.legend(['Cashflow',
               'Favourable Exercise',
               'Unfavourable Exercise',
               'Approx. of Continuation Value',
               'Out-of-the-money Continuation Value'])
    

def details():
    cashflow = np.maximum(strike - X[-1, :], 0)
    p = Polynomial.fit(X[-2, :], cashflow, 2)


def run():
    intermediate_results = []
    
    # given no prior exercise we just receive the payoff of a European option
    cashflow = np.maximum(strike - X[-1, :], 0.0)
    # iterating backwards in time 
    for i in reversed(range(1, X.shape[0] - 1)):
        x = X[i, :]
        # exercise value for time t[i]
        exercise = np.maximum(strike - x, 0.0)
        # boolean index of all in-the-money paths
        itm = exercise > 0.0
        # fit polynomial of degree 2
        fitted = Polynomial.fit(x[itm], cashflow[itm], 2)
        
        # approximate continuation value
        continuation = fitted(x)
        # boolean index where exercise is beneficial
        ex_idx = itm & (exercise > continuation)
        # update cashflows with early exercises
        cashflow[ex_idx] = exercise[ex_idx]
        
        intermediate_results.append((cashflow.copy(), x, fitted, continuation, exercise, ex_idx))






def generate_path_all(npath, params):
    """for i = 1..m
           dXi/Xi = mi.dt + voli.dWt with correlation of brownnians
        
    state = {
        "discount": paths_t[:, 0],
        "bond": paths_t[:, 1],
        "st": paths_t[:, 1],
        "bt": paths_t[:, 2],

    } 


    """
    masset = len( params["s0"])
    T      = params["T"]
    paths =  np.zeros((npath, masset+2,T+1), dtype="float64") 
    paths[:,0,:] = generate_path_discount(npath, params)
    paths[:,1,:] = generate_path_bond(npath, params)
    paths[:,2:,:] = generate_path(npath, params)
    return paths



def generate_path(npath, params):
    """for i = 1..m
           dXi/Xi = mi.dt + voli.dWt with correlation of brownnians
        
    state = {
        "discount": paths_t[:, 0],
        "st": paths_t[:, 1],
        "bt": paths_t[:, 2],
        "is_die": paths_t[:, 3],
    } 
    
    params = {    
           "s0"   :  [ 90.0, 100.0, 100.0],
           "v0"   :  [ 0.001, 0.1,  0.01],   
           "drift"   :[ 0.01, 0.01,  0.01],     
           "corr" :  [[ 1.0, 0.0][ 0.0, 1.0]]
           "T" : 10
        }

    np.zeros((nsimul, nasset, nstep+1)

    """
    from path_generator import  gbm_multi # (nsimul, nasset, nstep, T, S0, vol0, drift, correl, choice="all")
    # Generate MC paths, correlated Brownians
    s0, vol0, corr, drift = params['s0'], params['vol0'], params['corr'], params['drift']
    T = params['T']
    nasset = len(s0)

    allret, allpaths, _, corrbm_3d, correl_upper,_ = gbm_multi(nsimul=npath, nasset=nasset, 
        nstep=int(T), T=T, S0=s0, vol0=vol0, 
        drift=drift, correl=corr, choice="all")

    log("allpaths")
    return allpaths


def generate_path_discount(npath, params):
    """for i = 1..m
    generate_path_discount(7, params)

    """
    # Generate MC paths, correlated Brownians
    r, T = params['rate'], params['T']
   
    allpaths = np.zeros((npath,T+1), dtype="float32") 
    def one_path() :
      dt = np.arange(0,T+1)  ## range
      return  np.exp( -r*dt) * 100.0
    
    for k in range(npath) :
      allpaths[k,:]  = one_path()
    return allpaths


def generate_path_bond(npath, params):
    """for i = 1..m
    generate_path_discount(7, params)

    """
    # Generate MC paths, correlated Brownians
    r, T = params['rate'], params['T']
   
    allpaths = np.zeros((npath,T+1), dtype="float64") 
    def one_path() :
      dt = np.arange(0,T+1)  ## range
      return  np.exp( -r*(T-dt)) * 100.0
    
    for k in range(npath) :
      allpaths[k,:]  = one_path()
    return allpaths




def np_binomial_array(msample, nstep, proba ):
  ####  Binary array (msamole, nstep, with binomial events
  #  np_binomial_array( 13, 7, 0.3 )

  events = np.random.binomial( nstep, proba, size= msample)
  v = np.zeros((  msample , nstep    ), dtype="int32")
  
  for i, e in enumerate(events) :
       if e > 0 :  #no value is zero
          v[ i , e-1 ] = 1
  # print("np_binomial_array", v)
  return v


def generate_path_die(npath, params):
    """for i = 1..m        
        generate_path_die(10, { "proba_death": 0.01, "T": 7 })

    """
    proba = params["proba_death"]
    T     = params["T"]
    v     = np_binomial_array(npath, T+1, proba )
    return v







class proxy_table(object):
    ### USe a table to calcilate the lapse /expected value
    def __init__(self, model_dict):
        self.model = None
        if model_dict is None:
            self.model_dict = {"ix": list(np.arange(0, 10.0, 1.0)),
                               "iy": [0.6, 0.8, 0.9, 1.0, 1.1, 1.2, 1.4],
                               "array": np.array([[0.1, 0.1, 0.1, 0.2],
                                                  [0.1, 0.1, 0.1, 0.2],
                                                  [0.1, 0.1, 0.1, 0.2],
                                                  ])
                               }
        else:
            self.model_dict = model_dict

    def fit(self, paths):
        pass

    def predict(self, state):
        m = state['msample']

        proba_lapse = 0.3
        return np.random.Binomial(1, proba_lapse, m)

        is_lapse_list = []
        for k in range(0, m):
            ### Usage
            factor_01_list = [int(state.t)]  # time to maturity
            factor_02_list = np.round(state.S0 / 100.0, 2)  # Vector of moeyness

            M = self.model_dict['array']
            iM = self.model_dict['ix']
            jM = self.model_dict['iy']
            for i in range(M.shape[0]):
                for j in range(M.shape[1]):
                    if jM[i] < ix < jM[j + 1]:  ##3 Condition proba
                        pass

            is_lapse_list.append(np.random.Binomial(1, lapse_rate, 1))

            #### Reverse lapse value --> Decision Value

        """
        is_lapse      =  np_binomial_array(m, dT-1, proba_lapse )
        ### Cash Flow  ################################################

        for k in range(m) :
          for t in range(0, dT-1) :
            st = paths[:,t,0]

            if t == dT-1 :
                cf_sum = 100.0 + max(st/100-1, 0.0); break

            if is_lapse[t] == 1 :
                cf_sum = max(st[k]/100.0,0); break

            if is_die[t] == 1 :
                cf_sum = 150.0 + max(st[k]/100.0-1, 0.0); break


        cf[k] = cf_sum
        """



####################################################################################################
####################################################################################################


""""

   if name== "zero" :   # Zero correl
        s0    = np.ones((nasset)) * 100.0
        drift = np.ones((nasset,1)) * 0.0
        vol0  = np.ones((nasset,1)) * 0.2      
        vol0 =  np.array([[ 0.20, 0.15, 0.08 ]]).T 
        correl =  np.array([[100, 0 , 0 ],
                       [0,  100, -0 ],
                       [-0,  -0, 100 ],                ])*0.01




Self Supervised learning
Reinforcement Learning


Optimal Policy estimation using Neural Network and self-supervised apporach, application in lapse evaluation.







Estimation of Lapse : 
    relies 
   Actual financial impact problem,

   We decide to review a novel method to estimate and simulate Lapse for insurance related product.
   and compare wtih existing approach.


   Expensive

  User have sub-optimla (reason being retail vs instituatil), long term maturity, the impact can be signiticstive
  in the balance sheet, In addition, regulatory and ALM regulation pushes to provide better estimation of Lapses.


  LS : cannot handle a large number of factors (Exponential),
       Compute burden happens.
        Linearity of the basis function.


  LSTM 

A large number of users
Combine categorical and numerical data (conditional )




Backward induction










"""




