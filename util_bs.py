# -*- coding: utf-8 -*-
# %matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
import math
import scipy

####################################################################################################
"""
TODO :
   Write down plan
   Write Introduction, theoritical part.
   Write Down experiment list.

 params


generate_path


generate_cf
   state_calc
   lapse_cond
   cf_generation


generate_lapse_history

Approximation :
E[Vi+1]  = Sum(ak(ti). Sk(ti) )   : regression
E[Vi+1] = 


 proxy_table :
    table based

 proxy_regression


"""
####################################################################################################
####################################################################################################
VERBOSE = 1

def log(*arg) :
  if VERBOSE :
     for x in arg :
       print( x, globals().get(x), flush=True)


"""
https://quant.stackexchange.com/questions/43310/least-squares-monte-carlo-by-neural-network-estimator-for-pricing-american-optio



"""


####################################################################################################
####################################################################################################
"""
The third helper function pricing_function create the computational graph for the pricing 
and it generates for each call date the needed linear model 
and the training operator with the helper functions and store it in a list of training_functions.

"""

previous_exersies = 0
    npv = 0
    for i in range(number_call_dates-1):
        (input_x, input_y, train, w, b, y_hat) = get_continuation_function()
        training_functions.append((input_x, input_y, train, w, b, y_hat))
        X = feature_matrix_from_current_state(S_t[:, i])
        contValue = tf.add(tf.matmul(X, w),b)
        continuationValues.append(contValue)
        inMoney = tf.cast(tf.greater(E_t[:,i], 0.), tf.float32)
        exercise = tf.cast(tf.greater(E_t[:,i], contValue[:,0]), tf.float32) * inMoney 
        exercises.append(exercise)
        exercise = exercise * (1-previous_exersies)
        previous_exersies += exercise
        npv += exercise*E_t[:,i]
     
    # Last exercise date
    inMoney = tf.cast(tf.greater(E_t[:,-1], 0.), tf.float32)
    exercise =  inMoney * (1-previous_exersies)
    npv += exercise*E_t[:,-1]
    npv = tf.reduce_mean(npv)
    greeks = tf.gradients(npv, [S, r, sigma])
    



"""



"""
# Backward iteration to learn the continuation value approximation for each call date
        for i in range(n_excerises-1)[::-1]:
            (input_x, input_y, train, w, b, y_hat) = training_functions[i]
            y = exercise_values[:, i+1:i+2]
            X = paths[:, i]
            X = np.c_[X**1, 2*X**2-1, 4*X**3 - 3 * X]
            X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)
            for epoch in range(80):
                _ = sess.run(train, {input_x:X[exercise_values[:,i]>0], 
                                     input_y:y[exercise_values[:,i]>0]})
            cont_value = sess.run(y_hat, {input_x:X, 
                                     input_y:y})
            exercise_values[:, i:i+1] = np.maximum(exercise_values[:, i:i+1], cont_value)
            plt.figure(figsize=(10,10))
            plt.scatter(paths[:,i], y)
            plt.scatter(paths[:,i], cont_value, color='red')
            plt.title('Continuation Value approx')
            plt.ylabel('NPV t_%i'%i)
            plt.xlabel('S_%i'%i)
            
            
            
            









####################################################################################################
####################################################################################################
def blackscholes_mc(S=100, vol=0.2, r=0, q=0, ts=np.linspace(0, 1, 13), npaths=10):
    """Generate Monte-Carlo paths in Black-Scholes model.

    Returns
    -------
    paths: ndarray
        The Monte-Carlo paths.
    """
    nsteps = len(ts) - 1
    ts = np.asfarray(ts)[:, np.newaxis]
    W = np.cumsum(np.vstack((np.zeros((1, npaths), dtype=np.float),
                             np.random.randn(nsteps, npaths) * np.sqrt(np.diff(ts, axis=0)))),
                  axis=0)
    paths = np.exp(-0.5 * vol ** 2 * ts + vol * W) * S * np.exp((r - q) * ts)
    return paths


def blackscholes_price(K, T, S, vol, r=0, q=0, callput='call'):
    """Compute the call/put option price in the Black-Scholes model

    Parameters
    ----------
    K: scalar or array_like
        The strike of the option.
    T: scalar or array_like
        The maturity of the option, expressed in years (e.g. 0.25 for 3-month and 2 for 2 years)

    Returns
    -------
    price: scalar or array_like
        The price of the option.

    Examples
    --------
    >>> blackscholes_price(95, 0.25, 100, 0.2, r=0.05, callput='put')
    1.5342604771222823
    """
    F = S * np.exp((r - q) * T)
    v = np.sqrt(vol ** 2 * T)
    d1 = np.log(F / K) / v + 0.5 * v
    d2 = d1 - v
    try:
        opttype = {'call': 1, 'put': -1}[callput.lower()]
    except:
        raise ValueError('The value of callput must be either "call" or "put".')
    price = opttype * (F * norm.cdf(opttype * d1) - K * norm.cdf(opttype * d2)) * np.exp(-r * T)
    return price


def pwlinear_basis(xknots):
    """Basis that represent a piecewise linear function with given knots"""
    fs = [lambda x: np.ones_like(x, dtype=np.float), lambda x: x - xknots[0]]
    fs.extend([lambda x, a=xknots[i]: np.maximum(x - a, 0) for i in range(len(xknots))])
    return fs


def pwlinear_fit(xdata, ydata, xknots):
    """Fit a piecewise linear function with xknots to xdata and ydata"""
    fs = pwlinear_basis(xknots)
    A = np.vstack([f(xdata) for f in fs]).T
    ps = np.linalg.lstsq(A, ydata, rcond=None)[0]
    return ps, fs


def exponential_samples(n, lambd):
    # note that lambda is a reserved keyword in python, so we use lambd instead
    """Generate n samples from exponential distribution with rate lambd"""
    return -np.log(np.random.rand(n)) / lambd


def u_mat(x, K_mat=100.0):
    return np.maximum(K_mat - x, 0)


def u_D(x, K_mat=100.0):
    return np.maximum(K_mat - x, 0)


# longstaff schwartz algorithm for American pricing
def ls_test():
    npaths = 50
    S = 100
    vol = 0.3
    T = 10
    ts = np.linspace(0, T, int(np.round(T * 12)) + 1)
    dt = np.diff(ts)[0]
    K_mat = 90.0
    alpha = 3

    #### LS polynomial regression
    paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
    fun_approx = cf_future_approx_poly(paths, ts, )

    ####  Data
    npaths = 100
    paths = blackscholes_mc(S=S, vol=vol, r=0, q=0, ts=ts, npaths=npaths)
    val = option_price_01(paths, ts, fun_approx)
    print(val)


def cf_future_approx_poly(paths, ts,K_mat ):
    """
      # Create array that holds Polynomial coefficients from LS, put into vals
      CR(ti) = E[ Vi+1 /Xi ] =  Sum( ak(ti). X(ti)**k)

         VT = Payout(XT)
         Backward i=T-1...0  :
            Vi = h(Xi)   if h(Xi) > CR(ti)  ELSE  Vi+1

        (ak(ti) ,Xi**2) Polynomial for each time steps.

    """
    vals = []
    T = len(ts) - 2
    payoff = u_mat(paths[-1], K_mat)

    for i in range(T, -1, -1):
        payoff = payoff - (alpha * dt)
        xknots = np.linspace(np.percentile(paths[i], 1), np.percentile(paths[i], 99), 10)
        ps, fs = pwlinear_fit(paths[i], payoff, xknots)

        contval = payoff if i == T else sum([f(paths[i]) * p for (f, p) in zip(fs, ps)])
        exerval = (np.ones_like(paths[1, :], dtype=np.float) * 0.0) - (alpha * dt)

        is_lapse = exerval >= contval

        ### Vi = Vi+1 or h(Xi)
        payoff[is_lapse] = exerval[is_lapse]
        vals.append((fs, ps))

    vals = np.flip(vals, axis=0)
    return vals


def option_price_01(paths, ts, ):
    # independent simulation to obtain a lower bound
    payoff = u_mat(paths[-1], K_mat)  # Final Value
    for i in range(T, -1, -1):  # Backward
        payoff = payoff - (alpha * dt)
        if i == (T):
            contval = payoff
        else:
            ## Polynomial value
            contval = sum([f(paths[i]) * p for (f, p) in zip(vals[i][0], vals[i][1])])

        exerval = (np.ones_like(paths[1, :], dtype=np.float) * 0.0) - (alpha * dt)

        ## Lapse condition
        is_lapse = exerval >= contval
        payoff[is_lapse] = exerval[is_lapse]

    mc_mean = np.mean(payoff)
    return mc_mean


""""

   if name== "zero" :   # Zero correl
        s0    = np.ones((nasset)) * 100.0
        drift = np.ones((nasset,1)) * 0.0
        vol0  = np.ones((nasset,1)) * 0.2      
        vol0 =  np.array([[ 0.20, 0.15, 0.08 ]]).T 
        correl =  np.array([[100, 0 , 0 ],
                       [0,  100, -0 ],
                       [-0,  -0, 100 ],                ])*0.01


Self Supervised learning
Reinforcement Learning


Optimal Policy estimation using Neural Network and self-supervised apporach, application in lapse evaluation.







Estimation of Lapse : 
    relies 
   Actual financial impact problem,

   We decide to review a novel method to estimate and simulate Lapse for insurance related product.
   and compare wtih existing approach.


   Expensive

  User have sub-optimla (reason being retail vs instituatil), long term maturity, the impact can be signiticstive
  in the balance sheet, In addition, regulatory and ALM regulation pushes to provide better estimation of Lapses.


  LS : cannot handle a large number of factors (Exponential),
       Compute burden happens.
        Linearity of the basis function.


  LSTM 

A large number of users
Combine categorical and numerical data (conditional )




Backward induction










"""




